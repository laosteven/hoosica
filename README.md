# Hoosica: Music Web Browsing #
-------
Build a simple website with **HTML/CSS** and some **JavaScript** in order to observe and determine the dos and don'ts of website design.

![053.png](https://bitbucket.org/repo/XkLxL8/images/1045078186-053.png)

We built a music store website named **Hoosica**. Upon completion, we asked customers to perform various tasks and to share with us their thoughts and complaints about the general layout of the webpage.

# Objectives #
-------
* Learn about good and bad designs.
* Easy navigation.

# For more information #
-------
Visit the following website: [**Non-transactional websites conception** (TCH053)](https://cours.etsmtl.ca/tch053/) [*fr*]